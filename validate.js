module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function (age) {
        if (age.match(/^[0-9]+$/) && age >= 18 && age <= 100) {
            return true;
        }
        if (!(age.toString || age < 18 && age > 100)) {
            return true;
        }
        return false;
    },
    isPasswordValid: function (password) {
        if (password.toString) {
            return true;
        }
        if (password >= 8) {
            return true;
        }
        if (password.toUperCase >= 1) {
            return true;
        }
        if (password.match(/^[0-9]+$/) >= 3) {
            return true;
        }
        const regex = /([.*+?^=!:${}()|\[\]\/\\])/
        if (regex.test(password.length)) {
            return true;
        }
        return false;
    },
    isDateValid: function (day, month, year) {
        let month1 = [1, 3, 5, 7, 8, 10, 12];
        let month2 = [4, 6, 9, 11];
        const isSpecialYear = (year) => {
            if (!((year % 100 == 0) && (year % 400 != 0))) {
                return true;
            }
            if ((year % 100 == 0) && (year % 400 == 0)) {
                return true;
            }
        }
        
        if ((day >= 1 && day <= 31)) {
            return true;
        }
        if ((month >= 1 && month <= 12)) {
            return true;
        }
        if ((year >= 1970 && year <= 2020)) {
            return true;
        }
        if (checkMonth(day, month, year)) {
            return true;
        }
        const checkMonth = (day, month, year) => {
            if (month1.includes(month)) {
                if (day <= 31)
                    return true;
            } else if (month2.includes(month)) {
                if (day <= 30)
                    return true;
            } else {
                if (isSpecialYear(year)) {
                    if (day <= 29)
                        return true;
                } else {
                    if (day <= 28)
                        return true;
                }
            }
        }
        return false;
    }
}

