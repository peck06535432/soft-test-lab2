const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', () => {
    context('Function isUserNameValid', () => {
        it('Function prototype : boolean isUserNameValid(username: String)', () => {
            expect(validate.isUserNameValid('kob')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', () => {
            expect(validate.isUserNameValid('tu')).to.be.false;
        });
        it('ทุกตัวต้องเป็นตัวเล็ก', () => {
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('koB')).to.be.false;
        });
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', () => {
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });
    context('function isAgeValid', () => {
        it('Function prototype : boolean isAgeValid (age: String)', () => {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข', () => {
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', () => {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });
    context(' function isPasswordValid', () => {
        it('Function prototype : boolean isUserNameValid(password: String)', () => {
            expect(validate.isPasswordValid('00000000')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', () => {
            expect(validate.isPasswordValid('00000000')).to.be.true;
        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('00000O00')).to.be.true;
        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', () => {
            expect(validate.isPasswordValid('000POPOP')).to.be.true;
        });
        it('ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=\`{}[]:";<>?,./ อย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('!@#$%^&*()_+|~-=\`{}[]:";<>?,./')).to.be.true;
        });
    });
    context('Function isDateValid', () => {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', () => {
            expect(validate.isPasswordValid('day: Integer, month: Integer, year: Integer')).to.be.true;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid(1,1,2019)).to.be.true;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid(12,12,1980)).to.be.true;
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', () => {
            expect(validate.isDateValid(29,2,2000)).to.be.true;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกันตามรายการดังต่อไปนี้', () => {
            expect(validate.isDateValid(31,1,2000)).to.be.true;
            expect(validate.isDateValid(28,2,2000)).to.be.true;
            expect(validate.isDateValid(31,3,2000)).to.be.true;
            expect(validate.isDateValid(30,4,2000)).to.be.true;
            expect(validate.isDateValid(31,5,2000)).to.be.true;
            expect(validate.isDateValid(30,6,2000)).to.be.true;
            expect(validate.isDateValid(31,7,2000)).to.be.true;
            expect(validate.isDateValid(31,8,2000)).to.be.true;
            expect(validate.isDateValid(30,9,2000)).to.be.true;
            expect(validate.isDateValid(31,10,2000)).to.be.true;
            expect(validate.isDateValid(30,11,2000)).to.be.true;
            expect(validate.isDateValid(31,12,2000)).to.be.true;
        });
        it('ในกรณีของปี อธิกสุรทิน ให้คำนวนตามหลักเกณฑ์นี้ หารเลขปีด้วย 400 แล้วลงตัวไหม. ถ้าปีนั้นหารด้วย 100 ได้ แต่หารด้วย 400 ไม่ได้ เช่น 1900 แสดงว่า ไม่ใช่ ปีอธิกสุรทิน แต่ถ้าหารได้ทั้งคู่ แสดงว่าเป็นปีอธิกสุรทิน เพราะงั้นค.ศ. 2000 ก็เป็นปีอธิกสุรทินแน่นอน', () => {
            expect(validate.isDateValid(28,2,2000)).to.be.true;
        });
    });
});